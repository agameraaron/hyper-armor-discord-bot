# Hyper Armor Discord Bot

v0.7.1 "Clean Welcome Room"

A bot created for use with the Hyper Armor Discord server.

**Greet newly joined members to the server.**
  + Assign a member role to those who post the passphrase in the welcome 
    channel to ensure rules have been read & to keep bots out.
  + Keeps the welcome channel clean by deleting all posts not made by bots or 
    admins.
  + Will report in the log when a user has left the server.

**Manages a poll that members can partake in.**
  + Will announce choices at the start of a poll & results when it's ended.
  + Poll participation takes place over direct message with the bot to keep 
    voting private.

**Play & manage streaming audio from sources such as Youtube & Soundcloud.**
  + Can accept music player commands via both channel & direct message.