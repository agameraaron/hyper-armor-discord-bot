#Uses Python 3.7

#Hyper Armor Discord Bot
#"A bot created for use with the Hyper Armor Discord server."
#This code is MIT licensed.

#Made by AGamerAaron
#Thanks to DevDungeon & Lucas
#Both provide good Discord.py tutorials featured on Youtube.

#I built this with only my own server & personal needs in mind.
#If you wish to have a bot that works on multiple servers, this code will require some modification.

import discord
from discord.utils import get #for assigning roles
from discord.ext import commands #for music player
from discord.voice_client import VoiceClient #for figuring out where someone's voice client is via DM
#from discord.ext.commands import Bot #for music player
#For timed events
import asyncio
#For retrieving environment variables
import os
from dotenv import load_dotenv
load_dotenv()
#for dumping error log to file
import sys
#for timestamp info
from datetime import datetime
#to log for errors
import logging
#to allow stripping of punctuation easily
import string
#for file i/o
import json
#for streaming
import youtube_dl
#for background tasks (uptime log, check if still connected to voice)
from discord.ext import tasks, commands

#games
#import epet

#Common program settings
version = "v0.7.1"
version_name = "Clean Welcome Room"
debug = False

#Bot user environment variables
bot_token = os.getenv("BOT_TOKEN")
server_name = os.getenv("SERVER_NAME")
server_id = os.getenv("SERVER_ID")
admin_role_name = os.getenv("ADMIN_ROLE_NAME")
bot_role_name = os.getenv("BOT_ROLE_NAME")
bot_admins = os.getenv("BOT_ADMINS")
greeting_room_id = os.getenv("GREETING_ROOM_ID")
log_channel = os.getenv("LOG_CHANNEL")
welcome_channel = os.getenv("WELCOME_CHANNEL")
poll_announce_channel = os.getenv("POLL_ANNOUNCE_CHANNEL")
public_voice_channel = os.getenv("PUBLIC_VOICE_CHANNEL")
test_voice_channel = os.getenv("TEST_VOICE_CHANNEL")
stranger_role_name = os.getenv("STRANGER_ROLE_NAME")
member_role_name = os.getenv("MEMBER_ROLE_NAME")
stream_priveledged_role = os.getenv("STREAM_PRIVELEDGED_ROLE")
playing_game_message = os.getenv("PLAYING_GAME_MESSAGE")
log_directory = os.getenv("LOG_DIRECTORY")

#error log file
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename=(log_directory+'discord.log'), encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

#Bot object
client = commands.Bot(command_prefix = "!")
#I want a custom help page
client.remove_command('help')
client.remove_command('h')

#streaming audio player, if you are serving multiple servers all of these would need to be in associated arrays.
audio_player = ""
#audio_player options
loop = 0 #0 = no, 1 = loop single

#total time
#for data over longer periods of time, even between uptime and script crashes, saves minutes passed to a file
minutes_open = 0
days_open = 0

#direct message properties
dm_state = {} #menu state (main, epet_game)

#for keeping track of listeners on the proper voice channel
current_voice_channel = ""

#youtube-downloader
#Redirect errors from the youtube-dl library so the command line log
#   can stay clean
youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio',
    'outtmpl': log_directory+'%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': True,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)



async def help_page(message):
    output_message = """
:clipboard::rat: Hi! Here to help!
        
        General Commands
```!help                This help page.
!about                Learn about this bot.```
        Audio Streamer          (You have to be in a voice channel for the bot to join.)
```!play [url]            Plays an audio stream from many different sources.
                    Examples: Youtube, SoundCloud, LiveLeak, Nico Nico Douga
                    List of compatible streaming sites:
                    <https://github.com/ytdl-org/youtube-dl/blob/master/docs/supportedsites.md>
!pause                Pauses the audio stream. Resumes if already stopped.
!stop                Stops the audio stream.
!join                The bot will join the voice channel you are in (play automatically does joins).
!leave                The bot will leave the voice channel they are in.```
        Direct Message Commands
```hello(/hi/hey...)    Will greet you.
vote [choice number]    Participate in the latest poll.```"""
    #send a private message back to the channel received from
    await send_message(message.author,output_message)
    print("Help page sent to \""+str(message.author)+"\": \""+output_message+"\"")

@client.command()#(pass_context = True)
async def send_message(message_source,output_message):
    await message_source.send(output_message)

async def log_to_discord(log_message):
    server = client.get_guild(id=int(server_id))
    if log_channel != "": #if enabled
        await send_message(client.get_channel(int(log_channel)),(str(log_message.author)+", "+str(log_message.channel)+": \n"+str(log_message.content))) 
        
        

async def about_page(message):
    output_message = """
    ```Hyper Armor Discord Bot %s```
    This bot was created by AGamerAaron & it uses the MIT license.
    Developed primarily for use with the Hyper Armor Discord server.
    https://gitlab.com/agameraaron/hyper-armor-discord-bot/
    """%version
    await send_message(message.author,output_message)


async def timed_event(): #runs from minutely_open_log because it already has a minute long sleep, I guess I could make a more hublike function for both...
    #a minute
    minutes_open_plurality = ""
    if minutes_open != 1:
        minutes_open_plurality = "s"
    #await send_message(client.get_channel(int(log_channel)),("""Open for %s minute%s :warning:"""%(minutes_open,minutes_open_plurality)))
    
    #a day
    if minutes_open % 1440==0: #if perfectly divisible by amount of minutes in a day
        days_open = round(minutes_open/1440)
        days_open_plurality = ""
        if days_open != 1:
            days_open_plurality = "s"
        if days_open > 0:
            await send_message(client.get_channel(int(log_channel)),("""Open for %s day%s :warning:"""%(days_open,days_open_plurality)))
            #print("The bot has been open for "+str(minutes_open/1440)+" days.")


#log each minute passed into a file for total longterm time opened
async def minutely_open_log():
    while True:
        await asyncio.sleep(60)#seconds
        global minutes_open
        #read in current amount if minutes (if file exists)
        timelog_file = ""
        timelog_info = {}
        if minutes_open == 0: #still in initialized amount
            try:
                timelog_file = open(log_directory+'bot_timelog','r')
                timelog_info = timelog_file.read()
                timelog_info = json.loads(timelog_info) #convert from json to dictionary
                minutes_open = timelog_info['minutes_open']
                timelog_file.close()
            except:
                timelog_file = open(log_directory+'bot_timelog','w') #create new file
                minutes_open = 1
                timelog_info['minutes_open'] = minutes_open
                timelog_file.write(json.dumps(timelog_info)) #convert to json before storage
                timelog_file.close()
                print("A new time log file was created.")
            else: #try success
                timelog_file = open(log_directory+'bot_timelog','w')
                minutes_open += 1
                timelog_info['minutes_open'] = minutes_open
                timelog_file.write(json.dumps(timelog_info)) #convert to json before storage
                timelog_file.close()
        else:
            try:
                timelog_file = open(log_directory+'bot_timelog','r')
                timelog_info = timelog_file.read()
                timelog_info = json.loads(timelog_info) #convert from json to dictionary
                minutes_open = timelog_info['minutes_open']
                timelog_file.close()
            except:
                print("Error: timelog inaccessible.")
            else:
                timelog_file = open(log_directory+'bot_timelog','w')
                minutes_open += 1
                timelog_info['minutes_open'] = minutes_open
                timelog_file.write(json.dumps(timelog_info)) #convert to json before storage
                timelog_file.close()
        #also run any timed events from here
        await timed_event()

#run once an hour (for testing)
async def hourly_awake_log():
    hours_awake = 0
    hour_plurality = ""
    while True:
        await asyncio.sleep(3600)#seconds
        hours_awake += 1
        if hours_awake > 1:
            hour_plurality = "s"
        else:
            hour_plurality = ""
        await send_message(client.get_channel(int(log_channel)),("""Awake for %s hour%s :warning:"""%(hours_awake,hour_plurality)))

#run once a day
async def daily_awake_log():
    days_awake = 0
    while True:
        await asyncio.sleep(86400)#seconds
        days_awake += 1
        day_plurality = ""
        if days_awake != 1:
            day_plurality = "s"
        await send_message(client.get_channel(int(log_channel)),("Awake for %s day%s :tada:"%(days_awake,day_plurality)))

async def dm_main_menu(direct_message, message, user_id):
    msg = message.content
    #simple greeting, good way to see remotely if it's still running
    if await simple_in(msg,["hi","hello","hey","yo","whats up","greetings","howdy"]):
        await dm_hello(direct_message,message)
    elif await simple_in(msg,["v","vote"]):
        await vote(direct_message,message)
    elif await simple_in(msg,["poll new","p new","poll reset","p reset"]): #poll commands for bot admins
        await poll_new(direct_message,message)
    elif await simple_in(msg,["poll announce","p announce","poll begin","p begin","poll start","p start"]):
        await announce_poll(direct_message,message)
    elif await simple_in(msg,["poll edit","p edit"]):
        await poll_edit_question(direct_message,message)
    elif await simple_in(msg,["poll result","p result"]):
        await poll_results(direct_message,message)
    elif await simple_in(msg,["poll conclude","p conclude","poll end","p end"]):
        await announce_results(direct_message,message)
    elif await simple_in(msg,["epet_game"]): #secret, so it's specific ( but since it's not really a secret, you are free to alpha test if you see this and are in my server c: )
        """
        dm_state[user_id] = "epet_game"
        await epet_game.initialize(client, message.author, dm_state)
        #update dm_state via file
        await update_dm_state_file(user_id,"epet_game")
        """
        pass
        #still testing all this so it saves and loads DM states properly


async def update_dm_state_file(user_id,new_state):
    try:
        dm_state_file = open(log_directory+'dm_state','r')
    except:
        dm_state_file = open(log_directory+'dm_state','w') #create new file
        dm_state_file_dict[user_id:""]# = 0
        print("A new DM states file has been created for the first DM state switch.")
    else:
        dm_state_file_dict = dm_state_file.read()
        print("Replacing state "+str(dm_state)+" with "+new_state)
    dm_state_file_dict[user_id] = new_state
    #dm_state_file.write(dm_state)
    dm_state_file.close()

#simplify command input text so that the interpreter can read in more forms of input
async def simple_in(input_text,possible_triggers):
    success = False
    input_text = input_text.lower() #force lowercase
    #input_text = input_text.strip(string.punctuation)#strip all punctuation (probably bad)
    #("Command stripped: "+str(input_text))
    for trigger in possible_triggers:
        if input_text.startswith(trigger):
            return(True)
    return(False)


async def private_message_commands(direct_message, message):
    #if you have no save data, create it
    #print("DM State: "+str(dm_state))
    user_id = message.author.id
    try:
        dm_state[user_id]
    except KeyError:
        dm_state[user_id] = "main"
    if dm_state[user_id] == "main":
        await dm_main_menu(direct_message, message, user_id)
    elif dm_state[user_id] == "epet_game":
        await epet_game.main(client, message, dm_state)
    else:
        print("Unknown DM state: "+str(dm_state[user_id]))

async def public_message_commands(public_message, message):
    input_server = message.guild
    input_channel = message.channel
    
    #command received to assign Fan role
    if str(input_server) == server_name:
        if str(input_channel) == "welcome":
            #post any rodent
            introduction_message = "\nIf you need anything, just type \"!help\" and I can give you a list of bot commands.\n:sparkling_heart: Welcome to the server! :rat:"
            if "🐀" in message.content or "🐭" in message.content or "🐁" in message.content or "🐹" in message.content or "🐿" in message.content or "🐰" in message.content or "🐇" in message.content:
                #remove new role
                new_role = discord.utils.get(input_server.roles, name=stranger_role_name)
                await message.author.remove_roles(new_role)#await client.remove_roles(message.author, new_role)
                
                output_message = "Thanks!"+introduction_message
                #send a private message back to the client received from
                await send_message(public_message,output_message)
                #print("\"Member\" role assigned & DM sent to \""+str(message.author)+"\": \""+output_message+"\"")
                #greeter message in #freechat room
                greeter_message = ":confetti_ball: Let's welcome "+str(message.author.name)+" to the server! :confetti_ball:"
                await send_message(client.get_channel(int(greeting_room_id)),greeter_message)
                
                #assign Member role
                role = discord.utils.get(input_server.roles, name=member_role_name)
                await message.author.add_roles(role)#await client.add_roles(message.author, role)
                
                #joke answer, har har
            elif "🖱" in message.content:    
                #remove new role
                new_role = discord.utils.get(input_server.roles, name=stranger_role_name)
                await message.author.remove_roles(new_role)
                
                output_message = "Um, thanks!\nWe meant like a furry little mammal, but I guess the digital pointing device works too!"+introduction_message
                #send a private message back to the channel received from
                await send_message(public_message,output_message)
                #print("\"Member\" role assigned & DM sent to \""+str(message.author)+"\": \""+output_message+"\"")
                #greeter message in #freechat room
                greeter_message = ":confetti_ball: Let's welcome "+str(message.author.name)+" to the server! :confetti_ball:"
                await send_message(client.get_channel(int(greeting_room_id)),greeter_message)
                
                #assign role
                role = discord.utils.get(input_server.roles, name=member_role_name)
                await message.author.add_roles(role)
                await message.delete()
            elif ((admin_role in current_member.roles) or (bot_role in current_member.roles)) == False:
                output_message = "Make sure you read the rules channel for instructions on how to enter the server."
                await send_message(message.author,output_message) #direct message user
                
                current_member_id = int(message.author.id)
                current_member = input_server.get_member(current_member_id)
                await message.delete()
                
        else:
            print("Unaccepted command: "+message.content)
    else:
        print("Command attempted from incorrect server: "+message.content)

################################################################################
# Vote Polling
async def vote(direct_message,message):
    await log_to_discord(message)
    vote_input = message.content.split(" ")
    user_id = message.author.id
    err_instruction = "We only accept the entry number currently, which can be found next to each entry on the poll list."
    if len(vote_input) > 1: #if actually providing an argument
        vote_entry = vote_input[1]
        
        try:
            int(vote_entry) #if valid integer
        except:
            output_message = "Not a valid number. "+err_instruction
            await send_message(direct_message,output_message)
        else:
            vote_entry = int(vote_entry) #convert it to one then
            #open poll to see if there is already an entry for this person
            try:
                poll_file = open(log_directory+'pollbox','r')
                poll_box = poll_file.read()
                poll_box = json.loads(poll_box) #convert from json to dictionary
            except:
                poll_file = open(log_directory+'pollbox','w') #create new file
                poll_box = {}
                poll_file.write(json.dumps(poll_box))
                print("A new poll box has been created for the first vote.")
            poll_file.close()
            #print("Loaded poll box: "+str(poll_box))
            
            try:
                poll_file = open(log_directory+'pollinfo','r')
                poll_info = poll_file.read()
                poll_info = json.loads(poll_info) #convert from json to dictionary
            except:
                output_message = "No poll has been set up currently."
                await send_message(direct_message,output_message)
            else: #poll info found, can proceed
                poll_file.close()
                #print("Loaded poll info: "+str(poll_info))
            
                validated_vote = False
                #validate if it's actually a valid number choice available
                for votable in range(1,len(poll_info)):
                    if votable == vote_entry: #found it matches the number of something votable
                        if user_id in poll_box: #update vote
                            #poll_info_keys = poll_info.keys()
                            #poll_info_entry = poll_info_keys[vote_entry]
                            output_message = "Your vote has been updated to: "+str(poll_info[vote_entry])
                        else: #new vote
                            #poll_info_keys = poll_info.keys()
                            #poll_info_entry = poll_info_keys[vote_entry]
                            output_message = "You voted for: "+str(poll_info[vote_entry])
                        poll_box[user_id] = vote_entry #even if creating a new entry, it's the same command as updating an existing one
                        poll_file = open(log_directory+'pollbox','w')
                        poll_file.write(json.dumps(poll_box)) #convert to json before storage
                        poll_file.close()
                        validated_vote = True
                        #print("Vote accepted from: "+str(message.author))
                        #print("Poll box updated: "+str(poll_box))
                        
                        await send_message(direct_message,output_message)
                if validated_vote == False:
                    output_message = "Number out of range. "+err_instruction
                    await send_message(direct_message,output_message)
    else: #no argument provided
            output_message = "No entry marked for vote. "+err_instruction
            await send_message(direct_message,output_message)

async def poll_new(direct_message,msg):
    await log_to_discord(msg)
    user_id = msg.author.id
    if user_id == bot_admins: #user_id in bot_admins: #make sure a bot admin
        if msg.content.startswith("poll "):
            msg_input = msg.content.lstrip("poll new ")#
        elif msg.content.startswith("p "):
            msg_input = msg.content.lstrip("p new ")#
        msg_input = msg_input.split(",") #split all poll answers by comma (for now)
        #use message to store question
        if len(msg_input) > 2: #if has at least 2 answers (beyond question and answer 1)
            try:
                poll_file = open(log_directory+'pollinfo','r')
                last_poll_info = poll_file.read()
                last_poll_info = json.loads(last_poll_info) #convert from json to dictionary
                print("Last poll info being replaced: "+str(last_poll_info))
            except:
                poll_file = open(log_directory+'pollinfo','w') #create new file
                new_poll_info = {}
                poll_file.write(json.dumps(new_poll_info))
                print("New poll info file.")
            poll_file.close()
            
            #replace with new poll data
            output_message = "The poll question has been updated to: ```"+msg_input[0]+"```The results from the previous question have been reset."
            
            #store poll info into file
            for answer_num in range(1,len(msg_input)):
                #actual_answer_num = answer_num + 1
                output_message += ("\nAnswer "+str(answer_num)+": "+msg_input[answer_num])
            poll_file = open(log_directory+'pollinfo','w')
            poll_file.write(json.dumps(msg_input)) #convert to json before storage
            poll_file.close()
            #erase pollbox contents
            open(log_directory+'pollbox', 'w').close()
            
            #print("Poll info updated: "+str(poll_info))
            
            await send_message(direct_message,output_message)
        else:
            output_message = "Requires at least two answers."
            await send_message(direct_message,output_message)
    else:
        output_message = "You must be a bot admin to do that."
        await send_message(direct_message,output_message)

async def announce_poll(direct_message,msg):
    await log_to_discord(msg)
    user_id = msg.author.id
    if user_id == bot_admins:
        try:
            poll_file = open(log_directory+'pollinfo','r')
        except:
            poll_file = open(log_directory+'pollinfo','w') #create new file
            new_poll_info = {}
            poll_file.write(json.dumps(new_poll_info))
            print("New poll info file file created, but no announcement was made because it's empty.")
            poll_file.close()
        else:
            poll_info = poll_file.read()
            poll_info = json.loads(poll_info) #convert from json to dictionary
            print("Poll to be announced: "+str(poll_info))
            poll_file.close()
            output_message = ":ballot_box: Here's the latest poll! :ballot_box:\n"
            output_message += "    "+str(poll_info[0])
            for info_num in range(1,len(poll_info)):
                output_message += "\n\"vote "+str(info_num)+"\"    "+str(poll_info[info_num])
            #TODO reference here for .env (not critical if you forget)
            output_message += "\nTo participate in the poll, direct message @Hyper Armor Discord Bot#2910 & say \"vote\" followed by a space and the number of your selection." 
            server = client.get_guild(id=int(server_id))
            channel = discord.utils.get(server.channels,id=poll_announce_channel) #post to the same room the poll was announced in
            await send_message(channel,output_message)

async def poll_edit():
    print("Editting of polls is not implemented yet. Just make a new one for now ;)")

async def poll_results(direct_message,message):
    user_id = message.author.id
    if user_id == bot_admins:
        output_message = await read_poll_results()
        await send_message(direct_message,output_message)

async def announce_results(direct_message,message):
    user_id = message.author.id
    if user_id == bot_admins:
        output_message = await read_poll_results()
        server = client.get_guild(id=int(server_id))
        channel = discord.utils.get(server.channels,id=poll_announce_channel) #post to the same room the poll was announced in
        await send_message(channel,output_message)
        
async def read_poll_results():
#read in poll info
    poll_box = {}
    poll_info = {}
    try:
        poll_file = open(log_directory+'pollinfo','r')
        poll_info = poll_file.read()
        poll_info = json.loads(poll_info) #convert from json to dictionary
    except:
        poll_file = open(log_directory+'pollinfo','w') #create new file
        #poll_info = {}
        poll_file.write(json.dumps(poll_info))
        print("A new poll info file was created.")
    poll_file.close()
    #print("Loaded poll info: "+str(poll_info))
    
    #read in poll box
    try:
        poll_file = open(log_directory+'pollbox','r')
        poll_box = poll_file.read()
        poll_box = json.loads(poll_box) #convert from json to dictionary
    except:
        poll_file = open(log_directory+'pollbox','w') #create new file
        poll_box = {} #Fixes poll results?
        print("A new poll box file was created.")
    poll_file.close()
    #print("Loaded poll box: "+str(poll_box))
    
    #figure out total votes for each entry
    ballot = 0
    totals = []
    ballot_id = ""
    await zero_out_totals(totals,poll_info) #declerations to have less trouble doing so for each individual entry in the next segment
    
    poll_box_IDs = []
    poll_box_IDs = list(poll_box.keys())
    #for ballot_entry_number in range(0,len(poll_box)): #for each vote
    for evaluating_ballot_id_number in range(0,len(poll_box_IDs)):#find correct key
        #print("Compare ballot ID #"+str(evaluating_ballot_id_number))
        ballot_id = poll_box_IDs[evaluating_ballot_id_number]
        if poll_box[ballot_id]: #valid match 
            ballot_vote = int(poll_box[ballot_id]) #now associated with one of the answers
            #print("Ballot confirmed: "+str(ballot_vote)+" from "+str(ballot_id))
            totals[ballot_vote-1] += 1
    output_message = ("📣 Poll Results! 📣\n```"+str(poll_info[0])+"\n") #header
    total_votes = len(poll_box)
    output_message += "Total votes: "+str(total_votes)+"```\n" #total votes line
    for answer_entry_number in range(1,len(poll_info)): #output each entry line with total votes for each
        output_message += "    "+str(poll_info[answer_entry_number])+"  :  "+str(totals[answer_entry_number-1])+"\n" #append another line for each answer
    
    return(output_message)

async def zero_out_totals(totals,poll_info): #requires poll_info, a dictionary of the pollinfo file
    for poll_entry in range(1,len(poll_info)): #checks amount of answers
        totals.append(0)
    #print("Totals reset: "+str(totals))


# End of polling functions
################################################################################


################################################################################
# Audio streaming


@client.command()
async def join(ctx):#,channel):
    #log for admin review in case of
    
    await log_to_discord(ctx.message)
    
    if await voice_room_check(ctx):
        server = client.get_guild(id=int(server_id)) #hard coding it in
        #if summoned by an actual member of the server and if not already joined
        if server.voice_client is None:# or server.voice_client.is_connected() == False:
            if ctx.message.author in(server.members):#client.is_voice_connected(server) == False:
                channel = ctx.message.author.voice.channel #just pass the user directly since it's confirmed a valid channel by voice_room_check() already
                await channel.connect()


class Youtube_Downloader_Source(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.6):
        super().__init__(source, volume)
        self.data = data
        self.title = data.get('title')
        self.url = data.get('url')
        
    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))
        
        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]
        
        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

async def ensure_voice(ctx):
    if ctx.voice_client is None:
        if ctx.author.voice:
            await ctx.author.voice.channel.connect()
        else:
            await ctx.send("You are not connected to a voice channel.")
    elif ctx.voice_client.is_playing():
        ctx.voice_client.stop()

@client.command()
async def leave(ctx):
    #log for admin review in case of hijacking
    await log_to_discord(ctx.message)
    if await voice_room_check(ctx):
        server = client.get_guild(id=int(server_id))
        if ctx.message.author in(ctx.guild.members): #redundant with voice room check?: and server.voice_client is None == False:#voice_client.is_connected():#client.is_voice_connected(server) == True:
            voice_client = ctx.message.guild.voice_client
            await voice_client.disconnect()

@client.command()
async def pause(ctx):
    #log for admin review in case of hijacking
    await log_to_discord(ctx.message)
    if await voice_room_check(ctx):
        if ctx.voice_client.is_paused() == False:
            #print("Audio stream paused.")
            ctx.voice_client.pause()
        else:
            #print("Audio stream resumed.")
            ctx.voice_client.resume()

@client.command()
async def resume(ctx):
    #log for admin review in case of hijacking
    await log_to_discord(ctx.message)
    if await voice_room_check(ctx):
        if ctx.voice_client.is_paused() == True:
            #print("Audio stream resumed.")
            ctx.voice_client.resume()
        else:
            await send_message(ctx.message.author,"Stream is not paused.")
            

@client.command()
async def stop(ctx):
    if await voice_room_check(ctx):
        #log for admin review in case of hijacking
        await log_to_discord(ctx.message)
        await ctx.voice_client.disconnect()

@client.command()
async def loop(ctx):
    if await voice_room_check(ctx):
        #log for admin review in case of hijacking
        await log_to_discord(ctx.message)
        if loop == 0:
            loop = 1
        else:
            loop = 0

async def voice_room_check(ctx):
    server = client.get_guild(id=int(server_id))
    public_channel = discord.utils.get(server.channels,id=int(public_voice_channel))
    test_channel = discord.utils.get(server.channels,id=int(test_voice_channel))
    #print("'"+str(ctx.message.author.voice_channel)+"' compare with '"+str(voice_channel)+"'")
    
    #get Member data from User data in case of direct message
    voice_user = ctx.message.author.id
    voice_member = server.get_member(voice_user)
    try:
        voice_member_channel = voice_member.voice.channel
    except:
        pass
    #TODO make role tied to a role ID by using an environment variable
    if (voice_member in server.members): #if a part of the server
        voice_member_roles = voice_member.roles
        for vm_role in voice_member_roles:
            if vm_role.name == stream_priveledged_role: #if in the priveledged role
                if voice_member_channel is None:
                    await send_message(ctx.message.author,"You need to be in a voice channel to do that.")
                    return(False)
                elif str(voice_member_channel) == str(public_channel) or str(voice_member_channel) == str(test_channel):#if in a legitimate bot voice channel
                    #global current_voice_channel
                    current_voice_channel = voice_member_channel.id #update current voice channel
                    #print("Validated voice channel: "+str(current_voice_channel))
                    return(True)
                else:
                    await send_message(ctx.message.author,"You need to be in the "+str(public_channel)+" channel to do that.")
                    return(False)
        #if no roles match the priveledged one (no return before exit of for loop)
        await send_message(ctx.message.author,"You are not in the priveledged role to do that.")
        return(False)
    else:
        await send_message(ctx.message.author,"You need to be a member of a server with the bot in it to do that.")
        return(False)

@client.command()
async def play(ctx, url):
    #log for admin review in case of hijacking
    await log_to_discord(ctx.message)
    if await voice_room_check(ctx):
        server = client.get_guild(id=int(server_id))
        
        voice_user = ctx.message.author.id
        voice_member = server.get_member(voice_user)
        voice_member_channel = voice_member.voice.channel
        
        if ctx.message.author in(server.members) and server.voice_client is None:#voice_client.is_connected() == False:#(client.is_voice_connected(server) == False):
            channel = ctx.message.author.voice.channel #just pass the user directly since it's confirmed a valid channel by voice_room_check() already
            await channel.connect()
        
        await ensure_voice(ctx)
        
        player = await Youtube_Downloader_Source.from_url(url, loop=False)
        ctx.voice_client.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
        

async def strip_url(url):
    #print("to strip: "+str(url))
    #strip all carets (for users to be able to not display thumbnails but still submit a url)
    for search_character_index in range(len(url)-1):#for caret_found in url.rfind("<"):
        if url[search_character_index] == "<":
            url = url[:search_character_index] + url[search_character_index + 1:]
        elif url[search_character_index] == ">":
            url = url[:search_character_index] + url[search_character_index + 1:]
    if "&" in url: #strip out anything past an ampersand because it's either not needed or a playlist (which isn't supported yet)
        url_split = url.split("&",1) #only one split needed, throw everything after out
        url = url_split[0]
    #print("url strip result: "+str(url))
    return(url)

# End of audio streaming functions
################################################################################

async def reset_dm_mode(): #set back to home menu
    dm_state[user_id] = "main"

async def dm_hello(direct_message,message):
    output_message = "Hello, "+str(message.author.name)+"!"
    #send a private message back to the channel received from
    await send_message(direct_message,output_message)
    #print("DM greeting sent in return to \""+str(message.author)+"\": \""+output_message+"\"")

@client.event
async def on_member_join(member_joined):
    #assign new role
    server = client.get_guild(id=int(server_id))
    new_role = discord.utils.get(server.roles, name=stranger_role_name)
    await member_joined.add_roles(new_role)

@client.event
async def on_member_remove(member):
    log_to_discord("\""+member.user.name+"\" has left the server :c")

@client.event
async def on_message(message):
    if message.author != client.user: #if not my own bot self
        input_server = message.guild
        input_channel = message.channel
        if message.content.lower().startswith("!help"):
            await help_page(message)
        if message.content.startswith("!about"):
            await about_page(message)
        elif (str(input_channel).startswith("Direct Message with")): #TODO This could be more specific... I mean what if there is a public channel that starts with the name "Direct Message with "? So dumb, but so possible!!
            print("DM: \""+str(message.author)+"\": "+str(message.content))
            await private_message_commands(message.author,message)
        elif (str(input_server) == server_name) and (str(input_channel) == "welcome"):
            print("\"#"+str(input_channel)+" ("+str(message.author)+"): "+str(message.content)+"\"")
            await public_message_commands(message.author,message)
    await client.process_commands(message)

async def clean_welcome_room():
    server = client.get_guild(id=int(server_id))
    admin_role = discord.utils.get(server.roles,name=admin_role_name)
    bot_role = discord.utils.get(server.roles,name=bot_role_name)
    #delete_queue = []
    #ignore_queue = [] #for testing
    async for message in client.get_channel(int(welcome_channel)).history(limit=200):
        current_member_id = int(message.author.id)
        current_member = server.get_member(current_member_id)
        if current_member == None:
            #The scenario I ran into where this would happen is if the user was deleted, perhaps there are other situations in where it would return None for an existing message?
            #delete_queue.append("(Deleted User): "+message.content)
            await message.delete()
        elif (message.author == client.user) or (bot_role in current_member.roles) or (admin_role in current_member.roles):
            #ignore_queue.append(str(message.author)+": "+message.content)
            pass
        else:
            #delete_queue.append(str(message.author)+": "+message.content)
            await message.delete() #!scary looping deletion procedure!
    """ for debugging
    try:
        ignore_file = open(log_directory+'welcome_cleaning_ignored','rw')
        ignore_file.write(str(ignore_queue))
        print("Overwrote last 'welcome cleaning ignored' file.")
    except:
        ignore_file = open(log_directory+'welcome_cleaning_ignored','w') #create new file
        ignore_file.write(str(ignore_queue))
        print("A new 'welcome cleaning ignored' file has been created.")
    ignore_file.close()
    """
    """
    try:
        delete_file = open(log_directory+'welcome_cleaning_deleted','rw')
        delete_file.write(str(delete_queue))
        print("Overwrote last 'welcome cleaning deleted' file.")
    except:
        delete_file = open(log_directory+'welcome_cleaning_deleted','w') #create new file
        delete_file.write(str(delete_queue))
        print("A new 'welcome cleaning deleted' file has been created.")
    delete_file.close()
    """

async def voice_room_alone_check():
    alone_minute_total = 0
    while True: #since it's asynchronous, this can run in an infinite while loop with sleeping to keep it intermittent
        await asyncio.sleep(60)
        listeners = []
        server = client.get_guild(id=int(server_id))
        for current_server in client.guilds: #get the voice channel currently in
            voice_client = current_server.voice_client
            if voice_client is not None:#.is_connected():#client.is_voice_connected(current_server):
                listeners.clear() #rebuild listener list
                #global current_voice_channel
                current_voice_channel = voice_client.channel.id
                #print("Listening from room: "+str(current_voice_channel))
                voice_channel = discord.utils.get(current_server.channels,id=int(current_voice_channel))#=public_voice_channel)
                members = voice_channel.members
                listener_count = len(members)
                for member in members:
                    listeners.append(str(member))
                #print("Listeners: "+str(listeners))
                if listener_count <= 1: #counts self
                    alone_minute_total += 1
                    #print("Bot has been alone in voice chat for "+str(alone_minute_total)+"/3 minutes so far...")
                else: #resets
                    alone_minute_total = 0
                if alone_minute_total >= 3: #after 3 minutes being alone...
                    await voice_client.disconnect()
                    print("Disconnected from voice chat after being alone for 3 minutes.")
                    alone_minute_total = 0

@client.event
async def on_ready():
    
    print("Logged in: \""+str(client.user.name)+"\" : "+str(client.user.id))
    
    client.loop.create_task(minutely_open_log()) #keeps track of total time kept open overall
    client.loop.create_task(timed_event()) #sees if any events have been tripped over time
    client.loop.create_task(daily_awake_log()) #daily log check
    client.loop.create_task(voice_room_alone_check()) #checks voice chat to see if it's been left alone for too long
    
    #this only happens in real-time use when a new post is made, but this check ensures that if the bot was offline it makes up 
    #   for it when it comes back online.
    if debug:
        client.loop.create_task(hourly_awake_log()) #for stability testing
    if playing_game_message != "":
        await client.change_presence(status=discord.Status.idle, activity=discord.Game(playing_game_message))
    
    await clean_welcome_room() #all posts made in #welcome are scanned and any made by non-admins gets deleted

client.run(bot_token)
